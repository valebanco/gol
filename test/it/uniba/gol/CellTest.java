package it.uniba.gol;

import static org.junit.Assert.*;

import org.junit.Test;

public class CellTest {

	@Test
	public void cellShouldBeAlive() throws Exception {
		//Arrange
		Cell cell = new Cell(0, 0, true);
		//Assert
		assertTrue(cell.isAlive());
	}
	
	@Test(expected = CustomLifeException.class)
	public void cellShouldRaiseExceptionX() throws Exception {
		//Arrange
		new Cell(-1, 0, true);
	}
	
	@Test(expected = CustomLifeException.class)
	public void cellShouldRaiseExceptionY() throws Exception {
		//Arrange
		new Cell(0, -1, true);
	}
	
	@Test
	public void cellShouldBeDead() throws Exception {
		//Arrange
		Cell cell = new Cell(0, 0, true);
		cell.setAlive(false);
		//Assert
		assertFalse(cell.isAlive());
	}
	
	@Test
	public void cellShouldReturnX() throws Exception {
		//Arrange
		Cell cell = new Cell(5, 0, true);
		assertEquals(5,cell.getX());
	}
	
	@Test
	public void cellShouldReturnY() throws Exception {
		//Arrange
		Cell cell = new Cell(0, 5, true);
		assertEquals(5,cell.getY());
	}
	
	@Test
	public void cellShouldHaveAliveNeighbors() throws Exception {
		//Arrange
		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,false);
		cells[0][1] = new Cell(0,1,false);
		cells[0][2] = new Cell(0,2,true);
		
		cells[1][0] = new Cell(1,0,false);
		cells[1][1] = new Cell(1,1,true);
		cells[1][2] = new Cell(1,2,false);
		
		cells[2][0] = new Cell(2,0,true);
		cells[2][1] = new Cell(2,1,true);
		cells[2][2] = new Cell(2,2,false);
		
		cells[1][1].setNumberOfAliveNeighbors(cells);
		
		assertEquals(3, cells[1][1].getNumberOfAliveNeighbords());		
	}
	
	@Test
	public void aliveCellShouldSurvive() throws Exception {
		//Arrange
		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,false);
		cells[0][1] = new Cell(0,1,false);
		cells[0][2] = new Cell(0,2,true);
		
		cells[1][0] = new Cell(1,0,false);
		cells[1][1] = new Cell(1,1,true);
		cells[1][2] = new Cell(1,2,false);
		
		cells[2][0] = new Cell(2,0,true);
		cells[2][1] = new Cell(2,1,true);
		cells[2][2] = new Cell(2,2,false);
		
		cells[1][1].setNumberOfAliveNeighbors(cells);
		
		assertTrue(cells[1][1].willSurvive());		
	}
	
	@Test
	public void aliveCellWithThreeShouldSurvive() throws Exception {
		//Arrange
		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,false);
		cells[0][1] = new Cell(0,1,false);
		cells[0][2] = new Cell(0,2,true);
		
		cells[1][0] = new Cell(1,0,false);
		cells[1][1] = new Cell(1,1,true);
		cells[1][2] = new Cell(1,2,false);
		
		cells[2][0] = new Cell(2,0,true);
		cells[2][1] = new Cell(2,1,false);
		cells[2][2] = new Cell(2,2,false);
		
		cells[1][1].setNumberOfAliveNeighbors(cells);
		
		assertTrue(cells[1][1].willSurvive());		
	}
	
	@Test
	public void aliveCellShouldNotSurvive() throws Exception {
		//Arrange
		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,false);
		cells[0][1] = new Cell(0,1,false);
		cells[0][2] = new Cell(0,2,true);
		
		cells[1][0] = new Cell(1,0,false);
		cells[1][1] = new Cell(1,1,true);
		cells[1][2] = new Cell(1,2,false);
		
		cells[2][0] = new Cell(2,0,false);
		cells[2][1] = new Cell(2,1,false);
		cells[2][2] = new Cell(2,2,false);
		
		cells[1][1].setNumberOfAliveNeighbors(cells);
		
		assertFalse(cells[1][1].willSurvive());		
	}
	
	@Test
	public void deadCellShouldNotSurvive() throws Exception {
		//Arrange
		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,false);
		cells[0][1] = new Cell(0,1,false);
		cells[0][2] = new Cell(0,2,true);
		
		cells[1][0] = new Cell(1,0,false);
		cells[1][1] = new Cell(1,1,false);
		cells[1][2] = new Cell(1,2,false);
		
		cells[2][0] = new Cell(2,0,false);
		cells[2][1] = new Cell(2,1,false);
		cells[2][2] = new Cell(2,2,false);
		
		cells[1][1].setNumberOfAliveNeighbors(cells);
		
		assertFalse(cells[1][1].willSurvive());		
	}
	
	@Test
	public void aliveCellWithMoreThanThreeNeighbordsShouldDie() throws Exception {
		//Arrange
		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,false);
		cells[0][1] = new Cell(0,1,true);
		cells[0][2] = new Cell(0,2,true);
		
		cells[1][0] = new Cell(1,0,true);
		cells[1][1] = new Cell(1,1,true);
		cells[1][2] = new Cell(1,2,false);
		
		cells[2][0] = new Cell(2,0,false);
		cells[2][1] = new Cell(2,1,true);
		cells[2][2] = new Cell(2,2,false);
		
		cells[1][1].setNumberOfAliveNeighbors(cells);
		
		assertTrue(cells[1][1].willDie());		
	}
	
	@Test
	public void aliveCellWithLessThanTwoNeighbordsShouldDie() throws Exception {
		//Arrange
		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,false);
		cells[0][1] = new Cell(0,1,false);
		cells[0][2] = new Cell(0,2,false);
		
		cells[1][0] = new Cell(1,0,false);
		cells[1][1] = new Cell(1,1,true);
		cells[1][2] = new Cell(1,2,false);
		
		cells[2][0] = new Cell(2,0,false);
		cells[2][1] = new Cell(2,1,true);
		cells[2][2] = new Cell(2,2,false);
		
		cells[1][1].setNumberOfAliveNeighbors(cells);
		
		assertTrue(cells[1][1].willDie());		
	}
	
	@Test
	public void aliveCellShouldNotDie() throws Exception {
		//Arrange
		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,false);
		cells[0][1] = new Cell(0,1,true);
		cells[0][2] = new Cell(0,2,true);
		
		cells[1][0] = new Cell(1,0,false);
		cells[1][1] = new Cell(1,1,true);
		cells[1][2] = new Cell(1,2,false);
		
		cells[2][0] = new Cell(2,0,false);
		cells[2][1] = new Cell(2,1,true);
		cells[2][2] = new Cell(2,2,false);
		
		cells[1][1].setNumberOfAliveNeighbors(cells);
		
		assertFalse(cells[1][1].willDie());		
	}
	
	@Test
	public void deadCellShouldNotDie() throws Exception {
		//Arrange
		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,false);
		cells[0][1] = new Cell(0,1,true);
		cells[0][2] = new Cell(0,2,true);
		
		cells[1][0] = new Cell(1,0,false);
		cells[1][1] = new Cell(1,1,false);
		cells[1][2] = new Cell(1,2,false);
		
		cells[2][0] = new Cell(2,0,false);
		cells[2][1] = new Cell(2,1,true);
		cells[2][2] = new Cell(2,2,false);
		
		cells[1][1].setNumberOfAliveNeighbors(cells);
		
		assertFalse(cells[1][1].willDie());		
	}
	
	@Test
	public void deadCellShouldRevive() throws Exception {
		//Arrange
		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,false);
		cells[0][1] = new Cell(0,1,true);
		cells[0][2] = new Cell(0,2,true);
		
		cells[1][0] = new Cell(1,0,false);
		cells[1][1] = new Cell(1,1,false);
		cells[1][2] = new Cell(1,2,false);
		
		cells[2][0] = new Cell(2,0,false);
		cells[2][1] = new Cell(2,1,true);
		cells[2][2] = new Cell(2,2,false);
		
		cells[1][1].setNumberOfAliveNeighbors(cells);
		
		assertTrue(cells[1][1].willRevive());		
	}
	
	@Test
	public void aliveCellShouldNotRevive() throws Exception {
		//Arrange
		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,false);
		cells[0][1] = new Cell(0,1,true);
		cells[0][2] = new Cell(0,2,true);
		
		cells[1][0] = new Cell(1,0,false);
		cells[1][1] = new Cell(1,1,true);
		cells[1][2] = new Cell(1,2,false);
		
		cells[2][0] = new Cell(2,0,false);
		cells[2][1] = new Cell(2,1,true);
		cells[2][2] = new Cell(2,2,false);
		
		cells[1][1].setNumberOfAliveNeighbors(cells);
		
		assertFalse(cells[1][1].willRevive());		
	}
	
	@Test
	public void deadCellShouldNotRevive() throws Exception {
		//Arrange
		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,false);
		cells[0][1] = new Cell(0,1,false);
		cells[0][2] = new Cell(0,2,true);
		
		cells[1][0] = new Cell(1,0,false);
		cells[1][1] = new Cell(1,1,false);
		cells[1][2] = new Cell(1,2,false);
		
		cells[2][0] = new Cell(2,0,false);
		cells[2][1] = new Cell(2,1,true);
		cells[2][2] = new Cell(2,2,false);
		
		cells[1][1].setNumberOfAliveNeighbors(cells);
		
		assertFalse(cells[1][1].willRevive());		
	}
	
	@Test
	public void cellShouldNotBeNeighboard() throws Exception {
		//Arrange
		
		Cell cell1 = new Cell(0,0,false);
		Cell cell2 = new Cell(2,2,false);
		
		assertFalse(cell1.isNeighboard(cell2));		
	}
	
	@Test
	public void cellShouldNotBeNeighboardY() throws Exception {
		//Arrange
		
		Cell cell1 = new Cell(0,0,false);
		Cell cell2 = new Cell(0,2,false);
		
		assertFalse(cell1.isNeighboard(cell2));		
	}
}
